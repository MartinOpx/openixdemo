package com.openix.demo.rest.request;

import com.openix.demo.data.entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Setter @Getter
public class UserRequest {

    @NotEmpty
    private String name;

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;


    public User toEntity(){
        User user = new User();

        user.setName(getName());
        user.setUsername(getUsername());
        user.setPassword(getPassword());

        return user;
    }
}
