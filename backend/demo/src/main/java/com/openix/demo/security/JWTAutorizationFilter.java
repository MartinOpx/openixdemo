package com.openix.demo.security;

import com.openix.demo.data.entity.User;
import com.openix.demo.data.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import static com.openix.demo.configuration.Constants.*;

public class JWTAutorizationFilter extends BasicAuthenticationFilter {

    private UserRepository userRepository;

    public JWTAutorizationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
        super(authenticationManager);
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_AUTHORIZACION_KEY);

        if (header == null || !header.startsWith(TOKEN_BEARER_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request){

        String token = request.getHeader(HEADER_AUTHORIZACION_KEY);

        if (token != null){
             //se procesa el token y se recupera el usuario.
            String user = Jwts.parser()
                    .setSigningKey(SUPER_SECRET_KEY)
                    .parseClaimsJws(token.replace(TOKEN_BEARER_PREFIX, ""))
                    .getBody()
                    .getSubject();

//            String user = JWT.require(Algorithm.HMAC512(SUPER_SECRET_KEY.getBytes()))
//                    .build()
//                    .verify(token.replace(TOKEN_BEARER_PREFIX, ""))
//                    .getSubject();

            Jws<Claims> claims = Jwts.parser().setSigningKey(SUPER_SECRET_KEY).parseClaimsJws(token.replace(TOKEN_BEARER_PREFIX, ""));
            User userRepo = TokenContext.getUser();

            if(Objects.isNull(userRepo)){
                userRepo = this.userRepository.findById(((Integer) claims.getBody().get("UserID"))).get();
                TokenContext.setUser(userRepo);
            }

            TokenContext.setUserId(new Long((Integer) claims.getBody().get("UserID")));
            TokenContext.setCurrentToken(token);

            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }

            return null;
        }
        return null;
    }
}
