package com.openix.demo.rest.response;

import com.openix.demo.data.entity.User;
import org.springframework.data.domain.Page;

public class UserPagedResponse extends PageResponse<UserResponse> {

    public UserPagedResponse(Page<User> page){
        super(page);
        page.getContent().forEach(item -> data.add(new UserResponse(item)));
    }
}
