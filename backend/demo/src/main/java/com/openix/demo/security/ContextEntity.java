package com.openix.demo.security;

import com.openix.demo.data.entity.User;

public class ContextEntity {

    private Long userId;
    private User user;
    private String token;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userid) {
        this.userId = userid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
