package com.openix.demo.service;

import com.openix.demo.data.entity.Holiday;
import com.openix.demo.data.entity.User;
import com.openix.demo.data.repository.HolidayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.openix.demo.configuration.Constants.SERVICE_TO_GET_ALL_PAGEABLE;

@Service
public class HolidayService {

    private static final Logger logger = LoggerFactory.getLogger(HolidayService.class);
    private static final String ENTITY = "Holiday";

    @Autowired
    private HolidayRepository holidayRepository;

    public Holiday saveHoliday(Holiday holiday){
        return holidayRepository.save(holiday);
    }

    public Page<Holiday> findAll(Pageable pageable){
        logger.debug(SERVICE_TO_GET_ALL_PAGEABLE, ENTITY, pageable.getPageNumber(), pageable.getPageSize());
        return holidayRepository.findAll(pageable);
    }

    public void delete(Integer id){
        holidayRepository.deleteById(id);
    }

    public void update(){

    }
}
