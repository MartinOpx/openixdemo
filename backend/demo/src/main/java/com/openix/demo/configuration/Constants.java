package com.openix.demo.configuration;

public class Constants {

    // Spring Security

    public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
    public static final String TOKEN_BEARER_PREFIX = "Bearer ";

    // JWT

    public static final String ISSUER_INFO = "https://www.openix.com/";
    public static final String SUPER_SECRET_KEY = "b2ed38ddc864bce4cf58b1731682bb076bd4de01";
    public static final long TOKEN_EXPIRATION_TIME = 864_000_000; // 10 day

    // Log for Controller

    public static final String REQUEST_TO_GET_ALL = "Request to get all the {} Page: {} Size: {}";
    public static final String REQUEST_TO_GET = "Request to get a {} ID : {}";
    public static final String REQUEST_TO_PATCH = "Request to update {} ID: {} - {}";
    public static final String REQUEST_TO_SAVE = "Request to save {} : {}";
    public static final String REQUEST_TO_DELETE = "Request to delete {} ID : {}";

    public static final String REQUEST_TO_LOGIN = "Request to login Credentials : {}";
    public static final String REQUEST_TO_LOGOUT = "Request to logout";
    public static final String REQUEST_TO_GET_USER_BY_TOKEN = "Request to get user by token";

    // Log for Service

    public static final String SERVICE_TO_GET_ALL_PAGEABLE = "Service to get all the {} Page: {} Size: {}";
    public static final String SERVICE_TO_GET_ALL = "Service to get all {}";
    public static final String SERVICE_TO_GET = "Service to get a {} ID : {}";
    public static final String SERVICE_TO_SAVE = "Service to save a {} : {}";
    public static final String SERVICE_TO_UPDATE = "Service to update a {} ID: {}";
    public static final String SERVICE_TO_DELETE = "Service to delete a {} ID : {}";

    public static final String SERVICE_TO_GET_BY_NAME = "Service to get user by name: {}";

}