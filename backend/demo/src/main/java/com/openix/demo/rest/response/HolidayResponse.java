package com.openix.demo.rest.response;

import com.openix.demo.data.entity.Holiday;
import com.openix.demo.data.entity.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Getter
public class HolidayResponse implements Serializable {

    private Integer id;

    private LocalDate date;

    private String reason;

    public HolidayResponse(Holiday holiday){

        id = holiday.getId();
        date = holiday.getDate();
        reason = holiday.getReason();
    }
}
