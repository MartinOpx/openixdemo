package com.openix.demo.rest.response;

import com.openix.demo.data.entity.Holiday;
import org.springframework.data.domain.Page;

public class HolidayPageResponse extends PageResponse<HolidayResponse>{

    public HolidayPageResponse(Page<Holiday> page) {
        super(page);
        page.getContent().forEach(item -> data.add(new HolidayResponse(item)));
    }
}
