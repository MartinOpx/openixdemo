package com.openix.demo.rest.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class UserLoginRequest {

    @NotBlank
    @ApiModelProperty(notes = "Username", example = "martingz")
    private String username;

    @NotBlank
    @ApiModelProperty(notes = "password", example = "moregz27")
    private String password;
}
