package com.openix.demo.rest.controller;

import com.openix.demo.data.entity.Holiday;
import com.openix.demo.data.entity.User;
import com.openix.demo.data.repository.UserRepository;
import com.openix.demo.rest.request.HolidayRequest;
import com.openix.demo.rest.request.UserLoginRequest;
import com.openix.demo.rest.request.UserRequest;
import com.openix.demo.rest.response.HolidayPageResponse;
import com.openix.demo.rest.response.HolidayResponse;
import com.openix.demo.rest.response.UserPagedResponse;
import com.openix.demo.rest.response.UserResponse;
import com.openix.demo.service.HolidayService;
import com.openix.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.openix.demo.configuration.Constants.REQUEST_TO_GET_ALL;
import static com.openix.demo.configuration.Constants.REQUEST_TO_LOGIN;
@CrossOrigin(origins = "*")
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private static final String ENTITY = "User";

    @Autowired
    private UserService userService;

    @Autowired
    private HolidayService holidayService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
//
//    @GetMapping("/search")
//    public ResponseEntity<UserPagedResponse> getAll(@PageableDefault(page = 0, size = 10) Pageable pageable){
//        //logger.debug(REQUEST_TO_GET_ALL, ENTITY,pageable.getPageNumber(), pageable.getPageSize());
//        return ResponseEntity.ok(new UserPagedResponse(userService.findAll(pageable)));
//    }

    @PostMapping("/logIn")
    public ResponseEntity login(@RequestBody UserLoginRequest userLoginRequest){
        //logger.debug(REQUEST_TO_LOGIN, userLoginRequest);
        return ResponseEntity.ok(null);
    }

//    @PutMapping("/update")
//    public User updateUsers(@RequestBody User user){
//        userService.saveUser(user);
//        return user;
//    }

    @PostMapping("/signUp")
    public Integer saveUser(@RequestBody UserRequest userRequest){
        userRequest.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
        return new UserResponse(userService.saveUser(userRequest.toEntity())).getId();
    }







//    @PostMapping("/holiday/create")
//    public ResponseEntity<HolidayResponse> saveHoliday(@RequestBody HolidayRequest holidayRequest){
//        HolidayResponse holiday = new HolidayResponse(holidayService.saveHoliday(holidayRequest.toEntity()));
//        return ResponseEntity.ok(holiday);
//    }
//
//    @GetMapping("/holiday/search")
//    public ResponseEntity<HolidayPageResponse> getAllHoliday(@PageableDefault(page = 1, size = 10) Pageable pageable){
//        return ResponseEntity.ok(new HolidayPageResponse(holidayService.findAll(pageable)));
//    }
//
//    @DeleteMapping("/holiday/{id}")
//    public void delete(@PathVariable Integer id) {
//        holidayService.delete(id);
//    }


}
