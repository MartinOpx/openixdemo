package com.openix.demo.rest.request;


import com.openix.demo.data.entity.Holiday;
import com.openix.demo.data.entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.Date;

@Setter
@Getter
public class HolidayRequest {

    @NotEmpty
    private LocalDate date;

    @NotEmpty
    private String reason;

    public Holiday toEntity(){
        Holiday holiday = new Holiday();

        holiday.setDate(getDate());
        holiday.setReason(getReason());

        return holiday;
    }

}
