package com.openix.demo.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.openix.demo.data.entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Optional;

@Getter @Setter
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse implements Serializable {

    private Integer id;

    private String name;

    private String username;

    private String password;

    public UserResponse(User user){

        id = user.getId();
        name = user.getName();
        username = user.getUsername();
        password = user.getPassword();
    }

    public UserResponse(Optional<User> user){
        id = user.get().getId();
        name = user.get().getName();
        username = user.get().getUsername();
        password = user.get().getPassword();
    }
}
