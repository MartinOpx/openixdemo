package com.openix.demo.service;

import com.openix.demo.data.entity.Holiday;
import com.openix.demo.data.entity.User;
import com.openix.demo.data.repository.HolidayRepository;
import com.openix.demo.data.repository.UserRepository;
import com.openix.demo.rest.request.UserLoginRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.openix.demo.configuration.Constants.SERVICE_TO_GET_ALL_PAGEABLE;

@Service
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private static final String ENTITY = "User";

    @Autowired
    private UserRepository userRepository;



    public Page<User> findAll(Pageable pageable){
        logger.debug(SERVICE_TO_GET_ALL_PAGEABLE, ENTITY, pageable.getPageNumber(), pageable.getPageSize());
        return userRepository.findAll(pageable);
    }

    public User saveUser(User user){
        return userRepository.save(user);
    }

    public List<User> getUsers(){
        return userRepository.findAll();
    }

    public Optional<User> getUserLogin(String username, String password){
        return userRepository.findByUsernameAndPassword(username, password);
    }


}
