package com.openix.demo.data.repository;

import com.openix.demo.data.entity.Holiday;
import com.openix.demo.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HolidayRepository extends JpaRepository<Holiday, Integer> {
}
