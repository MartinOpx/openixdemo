package com.openix.demo.rest.response;

import lombok.Getter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class PageResponse<T> {

    protected List<T> data = new ArrayList<>();

    protected Integer pageNumber;
    protected Integer totalPages;
    protected Long totalOfElements;

    public PageResponse(Page<?> page) {

        pageNumber = page.getNumber();
        totalPages = page.getTotalPages();
        totalOfElements = page.getTotalElements();
    }
}
